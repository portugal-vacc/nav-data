from fastapi import APIRouter

from src.routes.aerodromes import aerodromes_router
from src.routes.charts import charts_router
from src.routes.runways import runways_router
from src.routes.sops import sops_router

router = APIRouter()

router.include_router(runways_router)
router.include_router(charts_router)
router.include_router(aerodromes_router)
router.include_router(sops_router)


