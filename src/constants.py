AIRPORT_RUNWAYS = {
    "LPBJ": ["01L", "19R", "01R", "19L"],
    "LPCS": ["17", "35"],
    "LPCR": ["11", "29"],
    "LPEV": ["01", "19", "07", "25"],
    "LPFR": ["10", "28"],
    "LPFL": ["18", "36"],
    "LPGR": ["09", "27"],
    "LPHR": ["10", "28"],
    "LPLA": ["15", "33"],
    "LPPT": ["02", "20"],
    "LPMA": ["05", "23"],
    "LPPI": ["09", "27"],
    "LPPD": ["12", "30"],
    "LPSO": ["03", "21"],
    "LPPR": ["17", "35"],
    "LPPS": ["18", "36"],
    "LPAZ": ["18", "36"],
    "LPVR": ["02", "20"],
    "LPMT": ["08", "26", "01", "19"]
}

ALL_AERODROMES = ["LPBJ", "LPCS", "LPCR", "LPEV", "LPFR", "LPFL", "LPGR", "LPHR", "LPLA", "LPPT", "LPMA", "LPPI",
                  "LPPD", "LPSO", "LPPR", "LPPS", "LPAZ", "LPVR", "LPBG", "LPBR", "LPCB", "LPCH", "LPCO", "LPFA",
                  "LPFC", "LPIN", "LPJF", "LPLZ", "LPMI", "LPMN", "LPMU", "LPPM", "LPPN", "LPSC", "LPSE", "LPSJ",
                  "LPSR", "LPVL", "LPVZ", "LP35", "LP39", "LP40", "LP41", "LP43", "LP65", "LPAB", "LPAF", "LPAG",
                  "LPAS", "LPBA", "LPBH", "LPCC", "LPCD", "LPCI", "LPCL", "LPCV", "LPDA", "LPER", "LPES", "LPFE",
                  "LPFO", "LPFX", "LPGA", "LPGO", "LPHB", "LPHC", "LPJB", "LPLE", "LPLO", "LPMB", "LPMC", "LPMD",
                  "LPMP", "LPMZ", "LPNV", "LPPA", "LPPB", "LPPH", "LPPJ", "LPSA", "LPSD", "LPSI", "LPSM", "LPSS",
                  "LPTH", "LPTM", "LPTT", "LPVC", "LPXR", "LP48", "LP49", "LP50", "LP51", "LP52", "LP53", "LP54",
                  "LP55", "LP57", "LP58", "LP59", "LP61", "LP62", "LP63", "LP64", "LP66", "LP68", "LP69", "LP71",
                  "LP75", "LP76", "LP78", "LP79", "LP80"]

AERODROME_WITH_CHARTS = [
    {
        "id":   "LPBJ",
        "name": "BEJA"
    },
    {
        "id":   "LPCS",
        "name": "CASCAIS"
    },
    {
        "id":   "LPCR",
        "name": "CORVO"
    },
    {
        "id":   "LPEV",
        "name": "EVORA"
    },
    {
        "id":   "LPFR",
        "name": "FARO"
    },
    {
        "id":   "LPFL",
        "name": "FLORES"
    },
    {
        "id":   "LPGR",
        "name": "GRACIOSA"
    },
    {
        "id":   "LPHR",
        "name": "HORTA"
    },
    {
        "id":   "LPLA",
        "name": "LAJES"
    },
    {
        "id":   "LPPT",
        "name": "LISBOA"
    },
    {
        "id":   "LPMA",
        "name": "MADEIRA"
    },
    {
        "id":   "LPPI",
        "name": "PICO"
    },
    {
        "id":   "LPPD",
        "name": "PONTA DELGADA"
    },
    {
        "id":   "LPSO",
        "name": "PONTE DE SOR"
    },
    {
        "id":   "LPPR",
        "name": "PORTO"
    },
    {
        "id":   "LPPS",
        "name": "PORTO SANTO"
    },
    {
        "id":   "LPAZ",
        "name": "SANTA MARIA"
    },
    {
        "id":   "LPVR",
        "name": "VILA REAL"
    },
    {
        "id":   "LPBG",
        "name": "BRAGANÇA"
    },
    {
        "id":   "LPBR",
        "name": "BRAGA"
    },
    {
        "id":   "LPCB",
        "name": "CASTELO BRANCO"
    },
    {
        "id":   "LPCH",
        "name": "CHAVES"
    },
    {
        "id":   "LPCO",
        "name": "COIMBRA"
    },
    {
        "id":   "LPFA",
        "name": "FERREIRA DO ALENTEJO"
    },
    {
        "id":   "LPFC",
        "name": "FIGUEIRA DOS CAVALEIROS"
    },
    {
        "id":   "LPIN",
        "name": "ESPINHO"
    },
    {
        "id":   "LPJF",
        "name": "LEIRIA"
    },
    {
        "id":   "LPLZ",
        "name": "LOUSÃ"
    },
    {
        "id":   "LPMI",
        "name": "MIRANDELA"
    },
    {
        "id":   "LPMN",
        "name": "AMENDOEIRA - MONTEMOR-O-NOVO"
    },
    {
        "id":   "LPMU",
        "name": "MOGADOURO"
    },
    {
        "id":   "LPPM",
        "name": "PORTIMÃO"
    },
    {
        "id":   "LPPN",
        "name": "PROENÇA-A-NOVA"
    },
    {
        "id":   "LPSC",
        "name": "SANTA CRUZ"
    },
    {
        "id":   "LPSE",
        "name": "SEIA"
    },
    {
        "id":   "LPSJ",
        "name": "SÃO JORGE"
    },
    {
        "id":   "LPSR",
        "name": "SANTARÉM"
    },
    {
        "id":   "LPVL",
        "name": "VILAR DE LUZ/MAIA"
    },
    {
        "id":   "LPVZ",
        "name": "VISEU"
    },
    {
        "id":   "LP35",
        "name": "SANTAREM HOSPITAL (HLP)"
    },
    {
        "id":   "LP39",
        "name": "LAGOS (HLP)"
    },
    {
        "id":   "LP40",
        "name": "MONCHIQUE (HLP)"
    },
    {
        "id":   "LP41",
        "name": "TORRES VEDRAS (HLP)"
    },
    {
        "id":   "LP43",
        "name": "PERNES (HLP)"
    },
    {
        "id":   "LP65",
        "name": "AGUIAR DA BEIRA (HLP)"
    },
    {
        "id":   "LPAB",
        "name": "ABRANTES HOSPITAL (HLP)"
    },
    {
        "id":   "LPAF",
        "name": "ALFRAGIDE (HLP)"
    },
    {
        "id":   "LPAG",
        "name": "ALBERGARIA-A-VELHA (HLP)"
    },
    {
        "id":   "LPAS",
        "name": "AMADORA HOSPITAL (HLP)"
    },
    {
        "id":   "LPBA",
        "name": "BARLAVENTO ALGARVIO HOSPITAL (HLP)"
    },
    {
        "id":   "LPBH",
        "name": "BRAGA HOSPITAL (HLP)"
    },
    {
        "id":   "LPCC",
        "name": "FUNCHAL HOSPITAL DR. NÉLIO MENDONCA (HLP)"
    },
    {
        "id":   "LPCD",
        "name": "SANTA COMBA DÃO (HLP)"
    },
    {
        "id":   "LPCI",
        "name": "COIMBRA HOSPITAL UNIVERSITÁRIO (HLP)"
    },
    {
        "id":   "LPCL",
        "name": "COVILHÃ (HLP)"
    },
    {
        "id":   "LPCV",
        "name": "COIMBRA HOSPITAL COVÕES (HLP)"
    },
    {
        "id":   "LPDA",
        "name": "MASSARELOS (HLP)"
    },
    {
        "id":   "LPER",
        "name": "ÉVORA (HLP)"
    },
    {
        "id":   "LPES",
        "name": "PONTA DELGADA - HOSPITAL (HLP)"
    },
    {
        "id":   "LPFE",
        "name": "FAFE (HLP)"
    },
    {
        "id":   "LPFO",
        "name": "FARO - HOSPITAL DISTRITAL (HLP)"
    },
    {
        "id":   "LPFX",
        "name": "CARNAXIDE HOSPITAL DE SANTA CRUZ (HLP)"
    },
    {
        "id":   "LPGA",
        "name": "GUARDA HOSPITAL (HLP)"
    },
    {
        "id":   "LPGO",
        "name": "ALMADA HOSPITAL (HLP)"
    },
    {
        "id":   "LPHB",
        "name": "HERDADE DA BRAVA (HLP)"
    },
    {
        "id":   "LPHC",
        "name": "CASCAIS HOSPITAL (HLP)"
    },
    {
        "id":   "LPJB",
        "name": "ALGÉS (HLP)"
    },
    {
        "id":   "LPLE",
        "name": "LEIRIA - HOSPITAL (HLP)"
    },
    {
        "id":   "LPLO",
        "name": "LOULÉ HELIPORT (HLP)"
    },
    {
        "id":   "LPMB",
        "name": "MORGADO DE APRA (HLP)"
    },
    {
        "id":   "LPMC",
        "name": "MACEDO DE CAVALEIROS (HLP)"
    },
    {
        "id":   "LPMD",
        "name": "MIRANDELA (HLP)"
    },
    {
        "id":   "LPMP",
        "name": "MAFRA HELIPORT (HLP)"
    },
    {
        "id":   "LPMZ",
        "name": "PORTO MONIZ HELIPORT (HLP)"
    },
    {
        "id":   "LPNV",
        "name": "TORRES NOVAS HOSPITAL (HLP)"
    },
    {
        "id":   "LPPA",
        "name": "PENAFIEL HOSPITAL (HLP)"
    },
    {
        "id":   "LPPB",
        "name": "PAREDES - BALTAR (HLP)"
    },
    {
        "id":   "LPPH",
        "name": "MATOSINHOS - HOSPITAL PEDRO HISPANO (HLP)"
    },
    {
        "id":   "LPPJ",
        "name": "BEJA HELIPORT (HLP)"
    },
    {
        "id":   "LPSA",
        "name": "SALEMAS (HLP)"
    },
    {
        "id":   "LPSD",
        "name": "SARDOAL HELIPORTO (HLP)"
    },
    {
        "id":   "LPSI",
        "name": "SINES HELIPORTO (HLP)"
    },
    {
        "id":   "LPSM",
        "name": "LISBOA HOSPITAL SANTA MARIA (HLP)"
    },
    {
        "id":   "LPSS",
        "name": "SANTA MARIA DA FEIRA - HOSPITAL (HLP)"
    },
    {
        "id":   "LPTH",
        "name": "TOMAR HOSPITAL (HLP)"
    },
    {
        "id":   "LPTM",
        "name": "BRAGANÇA HOSPITAL (HLP)"
    },
    {
        "id":   "LPTT",
        "name": "VISEU HOSPITAL (HLP)"
    },
    {
        "id":   "LPVC",
        "name": "VIANA DO CASTELO HOSPITAL (HLP)"
    },
    {
        "id":   "LPXR",
        "name": "VILA FRANCA DE XIRA HOSPITAL (HLP)"
    }
]
