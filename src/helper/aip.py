from enum import Enum
from typing import Optional, Tuple

import httpx

AIP_DATA_BASE_URL = "https://ais.nav.pt/wp-content/uploads/AIS_Files/eAIP_Current/eAIP_Online/eAIP/"
EVRFR_DATA_BASE_URL = "https://ais.nav.pt/wp-content/uploads/AIS_Files/eVFR_Current/eVFR_Online/eAIP/"


class Sources(str, Enum):
    AIP = "https://ais.nav.pt/wp-content/uploads/AIS_Files/eAIP_Current/eAIP_Online/eAIP/"
    EVFR_AD = "https://ais.nav.pt/wp-content/uploads/AIS_Files/eVFR_Current/eVFR_Online/eAIP/"
    EVFR_HLP = "https://ais.nav.pt/wp-content/uploads/AIS_Files/eVFR_Current/eVFR_Online/eAIP/"


def get_from_aip(airport_code: str, enroute: bool = False) -> Optional[str]:
    response = httpx.get(
        f"{AIP_DATA_BASE_URL}html/eAIP/{'LP-AD-2.' + airport_code.upper() + '-en-PT' if not enroute else 'LP-ENR-6-en-PT'}.html")
    if response.status_code == 404:
        return None
    return response.text


def get_from_vfr(airport_code: str, aerodrome: bool = True) -> Optional[str]:
    """ Aerodrome is true to look for AD.2 Where Local aerodromes are found.
        Otherwise, it will look for AD3 where heliports are found"""
    response = httpx.get(
        f"{EVRFR_DATA_BASE_URL}html/eAIP/LP-AD-{'2' if aerodrome else '3'}.{airport_code.upper()}-en-PT.html")
    if response.status_code == 404:
        return None
    return response.text


def content_manager(airport_code: str) -> tuple[None, None] | tuple[Sources, str]:
    source = Sources.AIP
    content = get_from_aip(airport_code, enroute=True) if airport_code == "enroute" else get_from_aip(airport_code)
    if not content:
        source = Sources.EVFR_AD
        content = get_from_vfr(airport_code, aerodrome=True)
        if not content:
            source = Sources.EVFR_HLP
            content = get_from_vfr(airport_code, aerodrome=False)
            if not content:
                return None, None
    return source, content
