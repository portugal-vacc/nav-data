import re

import httpx
import pandas as pd
from bs4 import BeautifulSoup, element
from stringcase import snakecase, alphanumcase

from src.helper.aip import AIP_DATA_BASE_URL, Sources
from src.helper.parsers.models import Aerodrome


def parse_charts(div, data_source: Sources) -> list:
    dfs = pd.read_html(str(div))
    dfs[0].dropna(subset=['Page'], inplace=True)
    return [{"name": line.get("Name", line.get("Title")), "link": line.get("Page").replace("../", data_source.value)}
            for line in dfs[0].to_dict(orient="records")]


def parse_simple_tables(div: element.Tag) -> dict:
    dfs = pd.read_html(str(div))
    data = dict(zip(dfs[0].to_dict(orient="list")[1], dfs[0].to_dict(orient="list")[2]))
    return {alphanumcase(snakecase(str(key).lower())): str(value) for key, value in data.items()}


# dataframe = dfs[0]
# print("dict", dataframe.to_dict(orient="dict"))
# print("list", dataframe.to_dict(orient="list"))
# print("series", dataframe.to_dict(orient="series"))
# print("split", dataframe.to_dict(orient="split"))
# print("tight", dataframe.to_dict(orient="tight"))
# print("records", dataframe.to_dict(orient="records"))
# print("index", dataframe.to_dict(orient="index"))

def parse_runway_physical_characteristics(div: element.Tag) -> list:
    dfs = pd.read_html(str(div))
    data = pd.concat([dfs[0], dfs[1]], axis=1).to_dict(orient="records")
    return [{alphanumcase(snakecase(key[0].lower())): str(value) for key, value in line_data.items()} for line_data in
            data]


def parse_row_col_tables(div: element.Tag) -> list:
    dfs = pd.read_html(str(div))
    initial_data = [dict(zip(record.keys(), record.values())) for record in dfs[0].to_dict(orient="records")]
    if [data for data in initial_data[0].keys() if type(data) != tuple]:
        return [{alphanumcase(snakecase(key.lower())): str(value) for key, value in line.items()} for line in
                initial_data][1:]
    return [{alphanumcase(snakecase(key[0].lower())): str(value) for key, value in line.items()} for line in
            initial_data]


def parse_eaip_ad_html(html_raw: str, data_source: Sources) -> Aerodrome:
    soup = BeautifulSoup(html_raw, 'html.parser')
    divs = soup.find_all("div")
    ad_name = soup.find("p", "ADName").text
    aerodrome = Aerodrome(icao=ad_name[:4], name=ad_name[7:].title())

    for div in divs:
        name = div.find('h4')
        if not name:
            continue
        key = name.text

        table = div.table
        if not table:
            continue
        if "Aerodrome geographical and administrative data" in key:
            aerodrome.geo_adm_data.update(parse_simple_tables(div))
        if "Operational hours" in key:
            aerodrome.operational_hours.update(parse_simple_tables(div))
        if "Handling services and facilities" in key:
            aerodrome.handling_services.update(parse_simple_tables(div))
        if "Passenger facilities" in key:
            aerodrome.passenger_facilities.update(parse_simple_tables(div))
        if "Rescue and fire fighting services" in key:
            aerodrome.rescue_fire_fight_services.update(parse_simple_tables(div))
        if "Meteorological information provided" in key:
            aerodrome.meteo_info.update(parse_simple_tables(div))
        if "Runway physical characteristics" in key:
            aerodrome.runways.extend(parse_runway_physical_characteristics(div))
        if "Declared distances" in key:
            aerodrome.distances.extend(parse_row_col_tables(div))
        if "ATS airspace" in key:
            aerodrome.airspace.update(parse_simple_tables(div))
        if "ATS communication facilities" in key:
            aerodrome.frequencies.extend(parse_row_col_tables(div))
        if "Radio navigation and landing aids" in key:
            aerodrome.navaids.extend(parse_row_col_tables(div))
        if re.search(r"(AD 2.24)|(AD 3.23)", key):
            aerodrome.charts.extend(parse_charts(div, data_source=data_source))

    return aerodrome


if __name__ == '__main__':
    airport_icao = 'lppt'
    url = f"{AIP_DATA_BASE_URL}html/eAIP/LP-AD-2.{airport_icao.upper()}-en-PT.html"
    response = httpx.get(url)
    print(parse_eaip_ad_html(response.text))
