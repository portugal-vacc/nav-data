from typing import Optional

from pydantic import BaseModel


class Aerodrome(BaseModel):
    icao: str
    name: str  # AD 2.1  Aerodrome location indicator and name
    geo_adm_data: Optional[dict] = {}  # AD 2.2  Aerodrome geographical and administrative data
    operational_hours: Optional[dict] = {}  # AD 2.3  Operational hours
    handling_services: Optional[dict] = {}  # AD 2.4  Handling services and facilities
    passenger_facilities: Optional[dict] = {}  # AD 2.5  Passenger facilities
    rescue_fire_fight_services: Optional[dict] = {}  # AD 2.6  Rescue and fire fighting services
    meteo_info: Optional[dict] = {}  # AD 2.11  Meteorological information provided
    runways: Optional[list] = []  # AD 2.12  Runway physical characteristics
    distances: Optional[list] = []  # AD 2.13  Declared distances
    airspace: Optional[dict] = {}  # AD 2.17  ATS airspace
    frequencies: Optional[list[dict]] = []  # AD 2.18  ATS communication facilities
    navaids: Optional[list] = []  # AD 2.19  Radio navigation and landing aids
    charts: Optional[list] = []  # AD 2.24  Charts related to the aerodrome
