import asyncio
import os

import httpx
import pypdfium2 as pdfium


async def get_async(url):
    async with httpx.AsyncClient(timeout=20) as client:
        return await client.get(url)


async def get_thumbnails(charts: list, force: bool = False, base_url: str = "/"):
    to_return = []
    urls = []
    for chart in charts:
        chart_id = chart.get("link").split("/eAIP_Online/eAIP/graphics/eAIP/")[-1]
        image_path = f'static/{chart_id}.jpg'
        if os.path.isfile(image_path) and not force:
            to_return.append(
                dict(name=chart.get("name"), id=chart_id, link=chart.get("link"), thumbnail=f"{base_url}{image_path}"))
        else:
            urls.append(chart.get("link"))
    if not urls:
        return to_return

    resps = await asyncio.gather(*map(get_async, urls))
    responses = (resp for resp in resps)

    for response in responses:
        chart = list(filter(lambda chart: chart.get("link") == response.url, charts))[0]
        chart_id = chart.get("link").split("/eAIP_Online/eAIP/graphics/eAIP/")[-1]
        image_path = f'static/{chart_id}.jpg'
        if os.path.isfile(image_path) and not force:
            to_return.append(
                dict(name=chart.get("name"), id=chart_id, link=chart.get("link"), thumbnail=f"{base_url}{image_path}"))
            continue
        if response.status_code != 200:
            to_return.append(dict(name=chart.get("name"), id=chart_id, link=None, thumbnail=None))
            continue
        pdf = pdfium.PdfDocument(input=response.content)
        page = pdf[0]
        pil_image = page.render(scale=1.5).to_pil()
        pil_image.save(fp=image_path)
        to_return.append(
            dict(name=chart.get("name"), id=chart_id, link=chart.get("link"), thumbnail=f"{base_url}{image_path}"))
    return to_return
