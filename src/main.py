from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from starlette.responses import RedirectResponse
from starlette.staticfiles import StaticFiles

from src.helper.aip import AIP_DATA_BASE_URL
from src import api

app = FastAPI(title="A Nav Data Pi", docs_url="/api/docs",
              description=f"A Pi for retriving automagically data from {AIP_DATA_BASE_URL}")

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/static", StaticFiles(directory="static"), name="static")
@app.get("/")
def redirect_to_docs():
    return RedirectResponse("/api/docs")


app.include_router(api.router, prefix="/api")
