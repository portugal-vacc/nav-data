from fastapi import APIRouter, HTTPException

from src.helper.aip import content_manager
from src.helper.parsers.aerodrome import parse_eaip_ad_html
from src.helper.parsers.models import Aerodrome

aerodromes_router = APIRouter()


@aerodromes_router.get("/aerodromes/{airport_icao}", response_model=Aerodrome)
async def get_aerodrome(airport_icao: str):
    source, content = content_manager(airport_icao)
    if not content:
        raise HTTPException(status_code=404, detail=f"{airport_icao} not found.")
    return parse_eaip_ad_html(content, data_source=source)
