import glob
import re
from collections import defaultdict

from fastapi import APIRouter, HTTPException
from starlette.requests import Request

from src.constants import ALL_AERODROMES, AERODROME_WITH_CHARTS
from src.helper.aip import content_manager
from src.helper.parsers.aerodrome import parse_eaip_ad_html, parse_charts
from src.helper.thumbnails import get_thumbnails

charts_router = APIRouter()


@charts_router.get("/charts/aerodromes")
def get_aerodromes():
    return AERODROME_WITH_CHARTS


@charts_router.get("/charts/aerodrome/{airport_icao}", response_model=list)
def get_charts(airport_icao: str):
    source, content = content_manager(airport_icao)
    if not content:
        raise HTTPException(status_code=404, detail=f"{airport_icao} not found.")
    return parse_eaip_ad_html(content, data_source=source).charts


@charts_router.get("/charts/{airport_icao}/thumbnails")
async def get_charts_thumbnails(request: Request, airport_icao: str, re_render: bool = False):
    source, content = content_manager(airport_icao)
    if not content:
        raise HTTPException(status_code=404, detail=f"{airport_icao} not found.")
    data = parse_charts(content, data_source=source) if airport_icao == "enroute" else \
        parse_eaip_ad_html(content, data_source=source).charts
    value = await get_thumbnails(data, re_render, str(request.base_url))
    return value


@charts_router.get("/charts/thumbnails")
def get_static_thumbnails():
    files_paths = glob.glob("./static/*.jpg")
    images = defaultdict(list)
    for file_path in files_paths:
        match = re.search(r"\w{2}_\w{2}_\d?_(\w{4})", file_path)
        name = match.group(1)
        images[name].append(file_path.replace("./", "/"))
    return images
