from typing import List

from fastapi import APIRouter

from src.constants import AIRPORT_RUNWAYS

runways_router = APIRouter()


@runways_router.get("/runways/{airport_icao_codes}", response_model=dict)
async def get_runways(airport_icao_codes: str):
    airports: List[str] = [airport for airport in airport_icao_codes.split(",")]
    runways = dict()
    for airport in airports:
        runways[airport.upper()] = AIRPORT_RUNWAYS.get(airport.upper(), ["Aerodrome not found."])
    return runways
