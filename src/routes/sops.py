from __future__ import annotations

from typing import Optional
import httpx
from fastapi import APIRouter
from pydantic import BaseModel

sops_router = APIRouter(prefix="/sops")


class Item(BaseModel):
    type: str
    name: str
    contents: Optional[list[Item]]


Item.update_forward_refs()


@sops_router.get("/available")
async def get_sops():
    response = httpx.get("https://docs.vatsim.pt/filesystem_structure.json")
    data = Item.parse_obj(response.json()[0])
    if data.type == "directory":
        return dict(
            sops=[dict(name=item.name.replace(".pdf", ""), url=f"https://docs.vatsim.pt/{item.name}") for item in
                  data.contents if ".pdf" in item.name])
